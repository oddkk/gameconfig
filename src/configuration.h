#ifndef GC_READER_H
#define GC_READER_H

#include <iostream>
#include <string.h>

namespace gameconfig
{
	class Configuration
	{
	public:
		Configuration(std::string path);
		~Configuration();

		void Store();
		bool HasChanged();
		
		bool   Read(std::string fieldName, bool def);
		int    Read(std::string fieldName, int def);
		double Read(std::string fieldName, double def);
		std::string Read(std::string fieldName, const char *def);
		std::string Read(std::string fieldName, std::string def);
		
		void Set(std::string fieldName, bool val);
		void Set(std::string fieldName, int val);
		void Set(std::string fieldName, double val);
		void Set(std::string fieldName, const char *val);
		void Set(std::string fieldName, std::string val);
	private:
		struct data;
		data *dt;
	};
}

#endif