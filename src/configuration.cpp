#include "configuration.h"
#include <iostream>
#include <string>
#include <fstream>
#include <map>

namespace gameconfig
{
	typedef std::map<std::string,std::string> strstrmap;

	struct Configuration::data 
	{
		strstrmap values;
		std::string path;
		bool hasChanged;
	};

	Configuration::Configuration(std::string path)
	{
		this->dt = new Configuration::data();
		this->dt->path = path;
		FILE *fs = fopen(path.c_str(),"r");

		if (fs == 0)
		{
			return;
		}

		unsigned char cChar;
		bool inVal = false;
		bool inComment = false;
		std::string fieldName = "";
		std::string value = "";
		
		while (fread(&cChar,sizeof(cChar),1,fs) > 0)
		{
			switch (cChar)
			{
			case '#':
				inComment = true;
				break;
			case '=':
				{
					inVal = true;
				}
				break;
			case '\n':
				{
					inVal = false;
					inComment = false;
					if (fieldName.size() > 0)
						dt->values.insert(std::pair<std::string,std::string>(fieldName,value));
					fieldName = "";
					value = "";
				}
				break;
			default:
				{
					if (!inVal)
					{
						fieldName += cChar;
					}
					else
					{
						value += cChar;
					}
				}
				break;
			}
		}
		if (inVal && fieldName.size() > 0)
			dt->values.insert(std::pair<std::string,std::string>(fieldName,value));
		fclose(fs);
		dt->hasChanged = false;
	}

	Configuration::~Configuration()
	{
		if (dt != 0)
		{
			delete dt;
			dt = 0;
		}
	}

	bool Configuration::HasChanged()
	{
		return dt->hasChanged;
	}

	void Configuration::Store()
	{
		if (!dt->hasChanged)
			return;
		FILE *fs = fopen(dt->path.c_str(),"w");
		if (fs == 0)
			return;
		for (strstrmap::iterator it = dt->values.begin(); it != dt->values.end(); it++)
		{
			std::string val = it->first + "=" + it->second + "\n";
			fwrite(val.c_str(),sizeof(unsigned char),val.size(),fs);
		}
		fclose(fs);
	}

	std::string Configuration::Read(std::string fieldName, std::string def)
	{
		strstrmap::iterator it = dt->values.find(fieldName);
		if (it == dt->values.end())
		{
			Set(fieldName,def);
			return def;
		}
		return it->second;
	}
	bool   Configuration::Read(std::string fieldName, bool def)    { return Read(fieldName,std::string(def ? "true" : "false")) == "true"; }
	int    Configuration::Read(std::string fieldName, int def)     { return std::atoi(Read(fieldName,std::to_string(def)).c_str()); }
	double  Configuration::Read(std::string fieldName, double def) { return (double)std::atof(Read(fieldName,std::to_string(def)).c_str()); }
	std::string Configuration::Read(std::string fieldName, const char *def) { return Read(fieldName,std::string(def)); }


	void Configuration::Set(std::string fieldName, std::string val)
	{
		dt->values[fieldName] = val;
		dt->hasChanged = true;
	}
	void Configuration::Set(std::string fieldName, bool val)   { Set(fieldName,std::string(val ? "true" : "false")); }
	void Configuration::Set(std::string fieldName, int val)    { Set(fieldName,std::to_string(val)); }
	void Configuration::Set(std::string fieldName, double val) { Set(fieldName,std::to_string(val)); }
	void Configuration::Set(std::string fieldName, const char *val) { Set(fieldName,std::string(val)); }

}
